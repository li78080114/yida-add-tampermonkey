import React from 'react';
import '../index.css';
import CopyIcon from "../../../../Icons/CopyIcon/index.jsx";
import {Message} from "@alifd/next";
import {GM_setClipboard} from "$";

const DINGBox = () => {
    const jsApiData = [
        {
            title: "钉钉JSAPI",
            description: '加载钉钉的JSAPI脚本',
            code: `this.utils.loadScript('https://g.alicdn.com/dingding/dingtalk-jsapi/3.0.25/dingtalk.open.js');`
        },{
            title: "钉钉端内判断",
            description: '由于很多钉钉JSAPI要求必须端内调用，因此在调用钉钉JSAPI时，需要提前先判断一下是否在钉钉端内',
            code: `export function isDingTalk() {\n return window.navigator && /dingtalk/i.test(window.navigator.userAgent)\n   }`
        }];

    return (
        <div className={'box-grid-1 yidaToolSkeletonBox'}>
            {jsApiData.map((item,index) => {
                return (
                    <div className={'jsApiItem_Box'} key={index}>
                        <div>
                            <div className={'jsApiItemTitle_Box'}>
                                <div className={'jsApiItemTitle'}>{item.title}</div>
                                <div className={'jsApiIcon_Box'} onClick={() => {
                                    if (item.code) {
                                        GM_setClipboard(item.code);
                                        Message.success('复制成功！')
                                    } else Message.error('复制失败，请联系开发者！')
                                }}>{CopyIcon('#3c99d8')}</div>
                            </div>
                            <div className={'jsApiItemDescription'}>{item.description}</div>
                        </div>
                    </div>
                )
            })}
        </div>
    );
};


export default DINGBox;