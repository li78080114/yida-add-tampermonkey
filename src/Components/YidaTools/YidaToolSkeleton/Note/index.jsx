import React from 'react';
import {Message} from "@alifd/next";
import CopyIcon from "../../../../Icons/CopyIcon/index.jsx";
//油猴引入复制
import {GM_setClipboard,unsafeWindow} from "$";
import moment from "moment";
const Note = () => {
    return (
        <div>
            <div className={'jsApiItem_Box'}>
                <div>
                    <div className={'jsApiItemTitle_Box'}>
                        <div className={'jsApiItemTitle'}>注 释</div>
                        <div className={'jsApiIcon_Box'} onClick={() => {
                                GM_setClipboard(`/** \n * 通过******，实现*****功能 。【方法详细解释】 \n * -------------------------------------------------------------------- \n * @author 【${unsafeWindow.loginUser.userName}】  \n * @date ${moment().format('YYYY-MM-DD')} \n * @param {Type} params={} 【入参数据】 \n * @return 【返回参数】 \n * */` );
                                Message.success('复制成功！')

                        }}>{CopyIcon('#3c99d8')}</div>
                    </div>
                    <div className={'jsApiItemDescription'}>
                        <p>{`/**`}</p>
                        <p>{`* 通过******，实现*****功能【方法详细解释】 `}</p>
                        <p>{`* ----------------------------------------------- `}</p>
                        <p>{`* @author 【作者】 `}</p>
                        <p>{`* @date 【${moment().format('YYYY-MM-DD')} 】`}</p>
                        <p>{`* @param {Type} params={} 【参数解释】`}</p>
                        <p>{`* @return 【返回参数】`}</p>
                        <p>{`* */`}</p>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default Note;