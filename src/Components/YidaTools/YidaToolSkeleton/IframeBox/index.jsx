import React, {Component} from 'react';
import {Button, Checkbox, Field, Form, Icon, Input, Message, Table,Radio,Typography } from "@alifd/next";
import {GM_setClipboard, unsafeWindow} from "$";
import * as XLSX from "xlsx";
import '../index.css';
import CopyIcon from "../../../../Icons/CopyIcon/index.jsx";

class iframeBox extends Component {
    state = {
        tableObj: [],
        jumpUrl:''
    }
    field = new Field(this);


    openDownloadDialog = (blob, fileName) => {
        if (typeof blob == 'object' && blob instanceof Blob) {
            blob = URL.createObjectURL(blob); // 创建blob地址
        }
        const aLink = document.createElement('a');
        aLink.href = blob;
        // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，有时候 file:///模式下不会生效
        aLink.download = fileName || '';
        let event;
        event = document.createEvent('MouseEvents');
        // console.log("unsafeWindow",unsafeWindow)
        event.initMouseEvent(
            'click',
            true,
            false,
            unsafeWindow,
            0,
            0,
            0,
            0,
            0,
            false,
            false,
            false,
            false,
            0,
            null
        );
        aLink.dispatchEvent(event);
    }

    workbook2blob = (workbook) => {
        // 生成excel的配置项
        const wopts = {
            // 要生成的文件类型
            bookType: 'xlsx',
            bookSST: false,
            type: 'binary',
        };
        const wbout = XLSX.write(workbook, wopts);

        // 将字符串转ArrayBuffer
        function s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
            return buf;
        }

        const blob = new Blob([s2ab(wbout)], {
            type: 'application/octet-stream',
        });
        return blob;
    }


    render() {
        const RadioGroup = Radio.Group;
        const {init, validate} = this.field;
        return (
            <div className={'yidaToolSkeletonBox'}>
                <h6 style={{margin: '8px 0'}}>查询表单</h6>
                <Form size={"small"}>
                    <Form.Item label={<h6>CorpId</h6>}>
                        {/*<h7>CorpId</h7>*/}
                        <Input placeholder={"corpId"}
                               className={"input-style"} {...init('corpId', {
                            initValue: unsafeWindow.pageConfig.corpId, rules: [{
                                required: true, message: "corpId 不能为空！"
                            }]
                        })}/>
                        {this.field.getError("corpId") ? (<span style={{
                            color: "red"
                        }}>{this.field.getError("corpId").join(",")}</span>) : ("")}
                    </Form.Item>
                    <Form.Item label={<h6>URL</h6>}>
                        <Input.TextArea placeholder={"url"} className={"input-style"} {...init('url', {
                            rules: [{
                                required: true, message: "url 不能为空！"
                            }]
                        })}/>
                        {this.field.getError("url") ? (<span style={{
                            color: "red"
                        }}>{this.field.getError("url").join(",")}</span>) : ("")}
                    </Form.Item>
                    <Form.Item>
                        {/*<Checkbox {...init('browser', {valueName: "checked1"})}>钉钉浏览器打开</Checkbox>*/}
                        {/*<Checkbox {...init('sidebar', {valueName: "checked2"})}>侧边栏打开</Checkbox>*/}
                        <RadioGroup
                            {...init('types',{initValue: "browser"})}
                            aria-labelledby="groupId"
                        >
                            <Radio id="browser" value="browser">
                                钉钉浏览器打开
                            </Radio>
                            <Radio id="sidebar" value="sidebar">
                                侧边栏打开
                            </Radio>
                        </RadioGroup>
                    </Form.Item>
                    <Form.Submit style={{width: "100%"}} onClick={() => {
                        validate();
                        // 请求表单

                        //地址拼接
                        let urls=this.field.getValue("url")
                        urls= urls.replace("$CORPID$", this.field.getValue("corpId"))
                        let jumpUrl=urls
                        if(this.field.getValue("types")=="browser"){
                            jumpUrl = "dingtalk://dingtalkclient/page/link?url=" + encodeURIComponent(urls + '&ddtab=true');
                        }
                        if (this.field.getValue("types")=="sidebar"){
                            jumpUrl = "dingtalk://dingtalkclient/page/link?url=" + encodeURIComponent(urls) + '&pc_slide=true';
                        }
                        this.setState({
                            jumpUrl
                        })
                        console.log("单选状态",this.field.getValue("types"),urls,this.state.jumpUrl)

                    }}>生成</Form.Submit>
                </Form>
                <hr style={{margin: '12px 0'}}/>
                <div className={'jsApiItemTitle_Box'}>
                    {/*<div className={'jsApiItemTitle'} >{this.state.jumpUrl}</div>*/}
                    <div className={'UrlItemTitle'} onClick={() => {
                        if (this.state.jumpUrl) {
                            GM_setClipboard(this.state.jumpUrl);
                            Message.success('复制成功！')
                        } else Message.error('复制失败，请联系开发者！')
                    }}>{this.state.jumpUrl}</div>
                    {
                        this.state.jumpUrl ? <div className={'jsApiIcon_Box'} onClick={() => {if (this.state.jumpUrl) {GM_setClipboard(this.state.jumpUrl);Message.success('复制成功！')} else Message.error('复制失败，请联系开发者！')}}>{CopyIcon('#3c99d8')}</div> : ''
                    }
                </div>
            </div>);
    }
}

export default iframeBox;